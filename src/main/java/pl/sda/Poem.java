package pl.sda;

import java.util.Scanner;

public class Poem {
    private String string;
    private Author creator;
    private int stropheNumbers;
    public Poem(Author creator, int stroheNumbers) {
        this.creator=creator;
        this.stropheNumbers=stropheNumbers;
    }
    public Author getCreator() {
        return creator;
    }
    public int getStropheNumbers(){
        return stropheNumbers;
    }
    @Override
            public String toString(){
        return "Poem{" +"creator=" + creator + ",stropheNumbers=" + stropheNumbers +'}';
    }

}
