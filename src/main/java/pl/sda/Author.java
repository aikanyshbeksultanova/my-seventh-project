package pl.sda;

import java.util.Scanner;

public class Author {
    private String surname;
    private String nationality;
    public Author (String surename, String nationality){
        this.surname=surname;
        this.nationality=nationality;
    }
    public String getSurname(){
        return surname;
    }
    public String getNationality(){
        return nationality;
    }
    @Override
    public String toString(){
        return "Author{" + "surname='"+surname + '\'' +",nationality='" + nationality + '\''+ '}';
    }

}
