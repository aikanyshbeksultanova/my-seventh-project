package pl.sda;

public class Main {
    public static void main(String[] args) {
    Author john = new Author ("Doe", "Canada");
    Author mark = new Author ("Twain", "USA");
    Author wieslawa = new Author ("Szymborska", "Poland");

    Poem goodOnePoem = new Poem (john, 10);
    Poem badOnePoem = new Poem(mark, 5);
    Poem excellentOnePoem = new Poem (wieslawa, 33);
    Poem[] poems = new Poem [3];
    poems[0] = goodOnePoem;
    poems[1] = badOnePoem;
    poems[2] = excellentOnePoem;
    int maxstropheNumbers= poems[0].getStropheNumbers();
    Poem bestPoem=null;
    for(Poem poem: poems){
        if (poem.getStropheNumbers() >= maxstropheNumbers){
            maxstropheNumbers=poem.getStropheNumbers();
            bestPoem=poem;
        }
    }
    if (bestPoem != null) {
            System.out.println("Best Poem:" +bestPoem);
            System.out.println("Best Author:" + bestPoem.getCreator());
        }
    }
}
